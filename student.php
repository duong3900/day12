<?php
$servername = "localhost";
$database = "studentlist";
$username = "root";
$password = "";
// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

mysqli_set_charset($conn, "utf8");

$sql = "SELECT id, name,gender, faculty, birthday, address, avartar FROM student";
$result = $conn->query($sql);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        #container-border{
            width: 1100px;
            height: 800px;
            margin-left: 350px;
            border: solid #006ccb 2px;        
        }
        #top{
            width: 400px;
            height: 120px;
            margin-left: 100px;
            margin-top: 50px;
            border: solid #fff; 
        }
        .search{
            background-color: #006ccb;
            height: 40px;
            width: 100px;
            border-radius: 8px;
            margin-left: 100px;
            margin-top: 20px;
            color: white;
        }
        .bottom{
            width: 100%;
            height: 400px;
            margin-top: 20px;
             
        }
        .add{
            background-color: #006ccb;
            height: 40px;
            width: 100px;
            border-radius: 8px;
            margin-left: 450px;
            margin-top: 20px;
            color: white; 

        }
        .table2{
            border: none;
            margin-top: 30px;
            border-spacing: 25px;
        }
        .click{
            padding: 5px 10px;
            background-color: #006ccb;
            color: #fff;
            border: 2px solid #006ccb;
            
        }
        .selectbox{
            width: 170px;
        }
        
    </style>
</head>
<body>

    <div id="container-border">
    <form action='' method='POST'>
        <div id="top">
            <table>
                <tr>
                    <td>Khoa</td>
                    <td><select class='selectbox' name = 'Khoa' value = ''>
                        <?php
                            $khoa = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                            foreach ($khoa as $key => $value) {
                            echo "
                            <option ";
                            echo isset($_POST['Khoa']) && $_POST['Khoa'] == $key ? "selected " : "";
                            echo "  value='" . $key . "'>" . $value  . "</option>";
                            }
                            $_SESSION['Khoa'] = $khoa[$_POST['Khoa']];
                        ?>
                        </select></td>
                </tr>
                <tr>
                    <td>Từ Khóa</td>
                    <td>
                        <input id="textin" type="text">
                    </td>
                </tr>
            </table>
            <button class="search">Tìm Kiếm</button>
        </div>
        <div class="bottom">
            <p>Số sinh viên tìm thấy:XXX</p>
            <?php
                    if (isset($_POST["signup"]))
                    { 
                        header("Location: signup.php");
                    }
                ?>
                <input class="add" type="submit" name='signup'  value="Thêm"></input>
            <div>
                <table class="table2">
                    <tr class="tr1">
                      <td><p>No</p></td>
                      <td><p>Tên sinh viên</p></td>
                      <td>
                        <p class="display">Giới tính</p>
                    </td>
                      <td><p>Khoa</p></td>
                      <td>
                        <p class="">Ngày sinh</p>
                    </td>
                    <td>
                        <p class="">Địa chỉ</p>
                    </td>
                    <td>
                        <p class="">Avartar</p>
                    </td>
                    <td>
                        <p class="">Action</p>
                    </td>
                      
                      
                      
                      
                    </tr>
                    <?php
                if ($result->num_rows > 0) {
                    // Load dữ liệu lên website
                    while($row = $result->fetch_assoc()) {
                    echo "<tr>
                           <td >
                                <p>" . $row["id"]. "</p>
                            </td>
                            <td>
                                <p class=''>" . $row["name"]. "</p>
                            </td>  
                            <td>
                                <p class=''>". $row["gender"]. "</p>
                            </td>
                            <td>
                                <p class=''>". $row["faculty"]. "</p>
                            </td>
                            <td>
                                <p class=''>". $row["birthday"]. "</p>
                            </td>
                            <td>
                                <p class=''>". $row["address"]. "</p>
                            </td>
                            <td>
                                <p class=''>". $row["avartar"]. "</p>
                            </td>
                            <td>
                            
                                <button class = 'custom click'>Xóa</button>
                                <button class = 'click'>Sửa</button>
                            </td>
                        </tr>
                            ";
                    }
                    } else {
                    echo "0 results";
                    }
                    $conn->close();
                    ?>
                  </table>
                </table>
            </div>
        </div>
    </div>
</body>
</html>